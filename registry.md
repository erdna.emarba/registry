# Création du projet

1. Créer un projet vide avec maven (archetype quickstart)
2. Ajouter un répertoire `src/main/resources`

# Couche de persistance

1. Configuration
   1. Ajouter les dépendances `hibernate-core`, le driver de postgres et `lombok`
   2. Ajouter un ficher `META-INF/persistence.xml`
2. Création des entités métiers dans un package `models`
   1. `Person` avec les attributs `String name`, `long id`, `Collection<Address> addresses`
   2. `Address` avec les attributs `String city`, `long id`, `Person person`
   3. Ajouter les annotations lombok et les annotations JPA (`@Entity`, `@Id`, ...)
3. Création de `HibernateUtil` contenant un `EntityManagerFactory` en singleton
4. Création des DAO
5. Test des DAO dans le main (ou dans des tests)