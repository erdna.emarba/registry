package fr.formation.annuaire.repositories;

import fr.formation.annuaire.models.Address;

public class AddressRepository extends GenericRepository<Address, Long> {

    public AddressRepository() {
        super(Address.class);
    }
}
