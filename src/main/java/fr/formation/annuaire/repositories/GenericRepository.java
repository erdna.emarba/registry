package fr.formation.annuaire.repositories;

import fr.formation.annuaire.utils.HibernateUtil;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;

import java.util.Collection;
import java.util.Optional;

public class GenericRepository <T, ID> {


    protected EntityManagerFactory entityManagerFactory = HibernateUtil.getEntityManagerFactory();

    private Class<T> clazz;

    public GenericRepository(Class<T> clazz) {
        this.clazz = clazz;
    }

    public Collection<T> findAll() {
        try (var em = entityManagerFactory.createEntityManager()) {
            return em.createQuery("from " + clazz.getName()).getResultList();
        }
    }

    public Optional<T> findById(ID id) {
        try (var em = entityManagerFactory.createEntityManager()) {
            return Optional.ofNullable(em.find(clazz, id));
        }
    }

    public T save(T element) {
        EntityTransaction t = null;
        try (var em = entityManagerFactory.createEntityManager()) {
            t = em.getTransaction();
            t.begin();
            em.persist(element);
            t.commit();
            return element;
        } catch (Exception e) {
            if (t != null && t.isActive())
                t.rollback();
            throw e;
        }
    }

    public T update(T element) {
        EntityTransaction t = null;
        try (var em = entityManagerFactory.createEntityManager()) {
            t = em.getTransaction();
            t.begin();
            element = em.merge(element);
            t.commit();
            return element;
        } catch (Exception e) {
            if (t != null && t.isActive())
                t.rollback();
            throw e;
        }
    }

    public void delete(T element) {
        EntityTransaction t = null;
        try (var em = entityManagerFactory.createEntityManager()) {
            t = em.getTransaction();
            t.begin();
            element = em.merge(element);
            em.remove(element);
            t.commit();
        } catch (Exception e) {
            if (t != null && t.isActive())
                t.rollback();
            throw e;
        }
    }

}
