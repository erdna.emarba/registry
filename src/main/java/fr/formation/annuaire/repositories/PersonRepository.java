package fr.formation.annuaire.repositories;

import fr.formation.annuaire.models.Person;

import java.util.Collection;

public class PersonRepository extends GenericRepository<Person, Long> {

    public PersonRepository() {
        super(Person.class);
    }

    public Collection<Person> findByNameLike(String name) {
        try (var em = entityManagerFactory.createEntityManager()) {
            var query = em.createQuery("from Person p where p.name like :name");
            query.setParameter("name", "%"+name+"%");
            return query.getResultList();
        }
    }
}
