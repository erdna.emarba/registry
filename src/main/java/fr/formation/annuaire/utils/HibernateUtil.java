package fr.formation.annuaire.utils;

import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HibernateUtil {

    @Getter
    private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("registry-postgres");

}
