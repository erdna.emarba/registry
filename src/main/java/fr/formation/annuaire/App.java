package fr.formation.annuaire;

import fr.formation.annuaire.models.Address;
import fr.formation.annuaire.models.Person;
import fr.formation.annuaire.repositories.AddressRepository;
import fr.formation.annuaire.repositories.PersonRepository;
import fr.formation.annuaire.utils.HibernateUtil;

import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        var personRepository = new PersonRepository();
        var addressRepository = new AddressRepository();

//        var addresses = List.of(
//                Address.builder().city("marseille").build(),
//                Address.builder().city("ajaccio").build()
//        );
//
//        var people = List.of(
//                Person.builder().name("andre").addresses(addresses).build(),
//                Person.builder().name("thomas").build(),
//                Person.builder().name("remi").build(),
//                Person.builder().name("stephane").build()
//        );
//
//        people.forEach(p -> p.getAddresses().forEach(a -> a.setPerson(p)));
//
//        people.forEach(p -> personRepository.save(p));

            var paris = addressRepository.findById(1L).get();
            var marseille = addressRepository.findById(52L).get();
            var ajaccio = addressRepository.findById(53L).get();

            var oldAndre = personRepository.findById(102L).get();
            var newAndre = Person.builder().id(102L).name("andre").addresses(List.of(ajaccio, paris)).build();

            oldAndre.getAddresses().stream()
                    .filter(a -> !newAndre.getAddresses().contains(a))
                    .forEach(a -> {
                        a.setPerson(null);
                        addressRepository.update(a);
                    });
            newAndre.getAddresses().stream()
                    .filter(a -> !oldAndre.getAddresses().contains(a))
                    .forEach(a -> {
                        a.setPerson(newAndre);
                        addressRepository.update(a);
                    });

            personRepository.update(newAndre);

    }
}
