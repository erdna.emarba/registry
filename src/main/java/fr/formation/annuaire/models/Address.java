package fr.formation.annuaire.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.*;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@SuperBuilder
@Entity
public class Address {

    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue
    private long id;

    private String city;

    @ToString.Exclude
    @ManyToOne
    private Person person;
}
