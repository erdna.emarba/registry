package fr.formation.annuaire.models;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.ArrayList;
import java.util.Collection;

@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@SuperBuilder
@Entity
public class Person {

    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue
    private long id;

    private String name;

    @Builder.Default
    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Collection<Address> addresses = new ArrayList<>();
}
